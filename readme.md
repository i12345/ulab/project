### 1. Add entried to /etc/hosts
```
127.0.0.1   timezones.local
127.0.0.1   backend.timezones.local
```

### 2. Create Source Directory     
`mkdir timezones`   

### 3. Clone Module
`git clone git@gitlab.com:i12345/ulab/project.git` 

### 4. Enter Project Directory
`cd project`

### 5. Initialize Submodule
`git submodule update --init`

### 6. Wake up Project
`cd docker`

### 7. Wake up Project
`docker-compose up`

### 8. Wait for images to build        
### 9. Wait for containers to startup      

### 10. Type Url in Broswer
Please note, after page loads, it takes about 5 sec for the timezones to load.
I should have add a progress bar or spinner of some sort
`http://timezones.local`


